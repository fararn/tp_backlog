package fr.usmb.m2isc.javaee.comptes.web;

import fr.usmb.m2isc.javaee.comptes.ejb.Traitement;
import fr.usmb.m2isc.javaee.comptes.jpa.Utilisateur;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/CreerUtilisateurServlet")
public class CreerUtilisateurServlet extends HttpServlet {

    @EJB
    private Traitement trt;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerUtilisateurServlet()
    {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *
     * On vérifie si le nom de l'utilisateur est déjà utilisé en BDD.
     * Si oui, alors il est attribué comme proprietaire de session
     * Sinon, il est crée puis attibué comme propriétaire de session.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userName = request.getParameter("username");

        Utilisateur user = trt.findUtilisateur(userName);

        if(user == null){
            user = trt.newUtilisateur(userName);
        }

        HttpSession session = request.getSession();

        session.setAttribute("user", user);

        request.setAttribute("utilisateur", user);

        request.getRequestDispatcher("/AfficherChoixAgence.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
