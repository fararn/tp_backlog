package fr.usmb.m2isc.javaee.comptes.web;

import fr.usmb.m2isc.javaee.comptes.ejb.Traitement;
import fr.usmb.m2isc.javaee.comptes.jpa.Agence;
import fr.usmb.m2isc.javaee.comptes.jpa.Backlog;
import fr.usmb.m2isc.javaee.comptes.jpa.Entree;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/AfficherBacklogAgenceServlet")
public class AfficherBacklogAgenceServlet extends HttpServlet {

    @EJB
    private Traitement trt;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficherBacklogAgenceServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int idAgency = Integer.parseInt(request.getParameter("idAgency"));

        Agence agence= trt.getAgence(idAgency);
        int idBacklog = agence.getBacklogid();
        List<Entree> listEntry = trt.findAllEntree(idBacklog);

        request.setAttribute("agence", agence);
        request.setAttribute("listEntry", listEntry);
        request.setAttribute("idBacklog", idBacklog);

        request.getRequestDispatcher("/AfficherBacklogAgence.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}