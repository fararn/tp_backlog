<%--
  Created by IntelliJ IDEA.
  User: adrien
  Date: 24/11/18
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Entree</title>
</head>
<body>
    <h1>Entrée : ${entry.nameEntree}</h1>
    <p> priorite : ${entry.priorite}</p>
    <p> Estimation : ${entry.estimation}</p>
    <p> description : ${entry.description}</p>
    <p> dateCreationEntree : ${entry.dateCreationEntree}</p>

    <h2> Commentaires : </h2>
    <c:forEach items="${listCom}"  var="commentaire">
        <p> Commentaire : ${commentaire.commentaire}</p>
        <p> Auteur : ${commentaire.auteur.nameUser}</p>
        <p> Crée le : ${commentaire.dateCreationCommentaire}</p>
    </c:forEach>

    <form action="CreerCommentaireEntreeServlet" method="post">
        Commentaire : <input type="text" name="commentaire">
        Auteur : <input type="text" name="user">
        <input type="hidden" name="idEntry" value="${entry.id}">
        <input type="submit" value="Ajouter un commentaire">
    </form>

    <p><a href="index.html" target="_blank">Revenir  la page principale</a></p>

</body>
</html>
