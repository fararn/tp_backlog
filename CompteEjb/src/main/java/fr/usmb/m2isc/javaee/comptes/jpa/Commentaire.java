package fr.usmb.m2isc.javaee.comptes.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@NamedQueries({
        @NamedQuery(name="allCommentaryEntry", query="SELECT c FROM Commentaire c WHERE c.entree=:entreeCourante"),
})
public class Commentaire implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    private String commentaire;

    @ManyToOne
    private Utilisateur auteur;

    @ManyToOne
    private Entree entree;
    /**
     * @since 1.8+
     */
    private LocalDate dateCreationCommentaire;

    public Commentaire(){}

    public Commentaire(String commentaire, Utilisateur auteur, Entree entree)
    {
        this.commentaire = commentaire;
        this.auteur = auteur;
        LocalDateTime dateAvecHeure = LocalDateTime.now();
        this.dateCreationCommentaire = dateAvecHeure.toLocalDate();
        this.entree = entree;
    }

    public int getId() {
        return id;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public LocalDate getDateCreationCommentaire() {
        return dateCreationCommentaire;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }


}
