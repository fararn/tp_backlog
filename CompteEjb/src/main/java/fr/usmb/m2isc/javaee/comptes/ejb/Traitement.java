package fr.usmb.m2isc.javaee.comptes.ejb;

import fr.usmb.m2isc.javaee.comptes.jpa.*;

import java.util.List;

public interface Traitement {

    //Opération lié a l'utilisateur
    Utilisateur newUtilisateur(String nameUser);
    Utilisateur getUtilisateur(int id);
    List<Utilisateur> findAllUtilisateur();
    Utilisateur findUtilisateur(String userName);

    //Opération lié a l'agence
    Agence newAgence(String nameAgence);
    Agence getAgence(int id);
    List<Agence> findAllAgence();

    //Opération lié aux entrées de backlog
    Entree newEntry(String nameEntree, int priorite, int estimation, String description, Backlog backlog);
    void deleteEntry(Agence agency, int idEntree);
    Entree getEntry(int id);
    void modifyPrioriteEntry(int id, int newPriorite);
    void modifyEstimationEntry(int id, int newEstimation);
    void modifyDescriptionEntry(int id, String newDescription);

    Backlog newBacklog();
    Backlog getBacklog(int id);
    void lierBacklogToAgency(int idAgency, int idBacklog);
    List<Entree> getEntrees(int idBacklog);

    List<Entree> findAllEntree(int idBacklog);

    //Opération lié au commentaire
    Commentaire newCommentaire(String commentaire, Utilisateur user, Entree entree);
    Commentaire getCommentaire(int id);
    void modifyCommentaire(int id, String newCommentaire);
    List<Commentaire> findAllCommentaireFromEntry(int idEntry);







}
