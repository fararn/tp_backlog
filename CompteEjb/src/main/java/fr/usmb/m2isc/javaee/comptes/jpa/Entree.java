package fr.usmb.m2isc.javaee.comptes.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name="allEntryBacklog", query="SELECT e FROM Entree e WHERE e.backlogPropritaire=:backlog"),
})
public class Entree implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    private String nameEntree;
    private LocalDate dateCreationEntree;
    private int priorite;
    private int estimation;
    private String description;

    @ManyToOne
    private Backlog backlogPropritaire;

    @OneToMany
    private List<Commentaire> commentaireEntree;

    public Entree(){}

    public Entree(String nameEntree, int priorite, int estimation, String description, Backlog backlogPropritaire)
    {
        this.nameEntree = nameEntree;
        this.priorite = priorite;
        this.estimation = estimation;
        this.description = description;

        LocalDateTime dateAvecHeure = LocalDateTime.now();
        this.dateCreationEntree = dateAvecHeure.toLocalDate();

        this.commentaireEntree = new ArrayList<>();

        this.backlogPropritaire = backlogPropritaire;
    }

    public int getId() {
        return id;
    }

    public String getNameEntree() {
        return nameEntree;
    }

    public LocalDate getDateCreationEntree() {
        return dateCreationEntree;
    }

    public int getPriorite() {
        return priorite;
    }

    public int getEstimation() {
        return estimation;
    }

    public String getDescription() {
        return description;
    }

    public List<Commentaire> getCommentaireEntree() {
        return commentaireEntree;
    }

    public void setNameEntree(String nameEntree) {
        this.nameEntree = nameEntree;
    }

    public void setPriorite(int priorite) {
        this.priorite = priorite;
    }

    public void setEstimation(int estimation) {
        this.estimation = estimation;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setDateCreationEntree(LocalDate dateCreationEntree) {
        this.dateCreationEntree = dateCreationEntree;
    }

    public Backlog getBacklogPropritaire() {
        return backlogPropritaire;
    }

    public void setBacklogPropritaire(Backlog backlogPropritaire) {
        this.backlogPropritaire = backlogPropritaire;
    }

    public void setCommentaireEntree(List<Commentaire> commentaireEntree) {
        this.commentaireEntree = commentaireEntree;
    }
}
